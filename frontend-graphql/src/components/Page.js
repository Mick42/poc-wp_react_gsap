import React, { Component } from 'react';
import { withApollo } from 'react-apollo';
import gql from 'graphql-tag';

import { Transition } from "react-transition-group";
import { TweenMax } from "gsap/all";

const startState = { autoAlpha: 0, y: -50 };


/**
 * GraphQL page query that takes a page slug as a uri
 * Returns the title and content of the page
 */
const PAGE_QUERY = gql`
  query PageQuery($uri: String!) {
    pageBy(uri: $uri) {
      title
      content
    }
  }
`;

/**
 * Fetch and display a Page
 */
class Page extends Component {
  state = {
    page: {
      title: '',
      content: '',
    },
  };

  componentDidMount() {
    this.executePageQuery();
  }

  /**
   * Execute page query, process the response and set the state
   */
  executePageQuery = async () => {
    const { match, client } = this.props;
    let uri = match.params.slug;
    if (!uri) {
      uri = 'welcome';
    }
    const result = await client.query({
      query: PAGE_QUERY,
      variables: { uri },
    });
    const page = result.data.pageBy;
    this.setState({ page });
  };

  render() {
    const { page } = this.state;
    return (
      <div>
        <div className="pa2">
          <h1>{page.title}</h1>
        </div>
        <div>
          <InsidePage animate={page.title != ""} content={page.content} />
        </div>

      </div>
    );
  }
}

class InsidePage extends Component {
  render() {
    return (
      <Transition
        unmountOnExit
        in={this.props.animate}
        timeout={1000}
        onEnter={node => TweenMax.set(node, startState)}
        addEndListener={(node, done) => {
          TweenMax.to(node, 0.5, {
            autoAlpha: this.props.animate ? 1 : 0,
            y: this.props.animate ? 0 : 50,
            onComplete: done
          });
        }}
      >

        <div
          // eslint-disable-next-line react/no-danger
          dangerouslySetInnerHTML={{
            __html: this.props.content,
          }}


        />

      </Transition>
    );
  }
}

export default withApollo(Page);