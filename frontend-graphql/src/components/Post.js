import React, { Component } from 'react';
import { withApollo } from 'react-apollo';
import gql from 'graphql-tag';
import { Transition } from "react-transition-group";
import { TweenMax, Linear } from "gsap/all";
import { getStoreKeyName } from 'apollo-utilities';

const startState = { autoAlpha: 0.1};

/**
 * GraphQL post query that takes a post slug as a filter
 * Returns the title, content and author of the post
 */
const POST_QUERY = gql`
  query PostQuery($filter: String!) {
    postBy(slug: $filter) {
      title
      content
      author {
        nickname
      }
    }
  }
`;

/**
 * Fetch and display a Post
 */
class Post extends Component {
  state = {
    post: {
      title: '',
      content: '',
      author: {
        nickname: '',
      },
    },
  };

  componentDidMount() {
    this.executePostQuery();
  }

  /**
   * Execute post query, process the response and set the state
   */
  executePostQuery = async () => {
    const { match, client } = this.props;
    const filter = match.params.slug;
    const result = await client.query({
      query: POST_QUERY,
      variables: { filter },
    });
    const post = result.data.postBy;
    this.setState({ post });
  };

  render() {
    const { post } = this.state;
    return (
      <div>


        <AnimationTitle animate={post.title != ""} content={post} />


        <div
          // eslint-disable-next-line react/no-danger
          dangerouslySetInnerHTML={{
            __html: post.content,
          }}
        />
        <div>Written by {post.author.nickname}</div>
      </div>
    );
  }
}


class AnimationTitle extends Component {
  render() {
    return (

      <Transition
        unmountOnExit
        in={this.props.animate}
        timeout={1000}
        onEnter={node => TweenMax.set(node, startState)}
        addEndListener={(node, done) => {
          TweenMax.to(node, 0.5, {
            autoAlpha: this.props.animate ? 1 : 0,
            onComplete: done,
            ease: Linear.ease,
          });
        }}
        onExit={(node, done) => {
          TweenMax.to(node, 0.5, {
           autoAlpha : 0,
            x: -100,
            backgroundColor : "green",
            onComplete: done,
            ease: Linear.ease,
          });
        }}
      >
        <div className="testTitle">
          <h1>{this.props.content.title}</h1>
        </div>

      </Transition>
    )
  }
}

export default withApollo(Post);
