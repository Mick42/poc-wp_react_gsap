import React, { useEffect, useRef } from 'react';
import { Switch, Route, BrowserRouter } from 'react-router-dom';
import { Transition, TransitionGroup, CSSTransition } from "react-transition-group";
import Header from './Header';
import Footer from './Footer';
import Home from './Home';
import Login from './Login';
import Search from './Search';
import Page from './Page';
import Post from './Post';
import Category from './Category';

const App = () => {
  

  return (
    
    <div className="center">
      <Header />
      <div className="pa1 padding table">
        <Switch>
          <Route exact path="/" >
{ ({ match }) => <Home match={match} show={match !== null} /> }
          </Route>
          <Route exact path="/login" component={Login} />
          <Route exact path="/search" component={Search} />
          <Route exact path="/page/:slug">
          { ({ match }) => <Page match={match} show={match !== null} /> }
         </Route>
          <Route exact path="/post/:slug" component={Post} />
          <Route exact path="/category/:slug" component={Category} />
        </Switch>
      </div>
      <Footer />
    </div>
  );
}


export default App;




