Installation du projet : 

1-Cloner le projet : `git@gitlab.com:Mick42/poc-wp_react_gsap.git`

2-Afin d'éviter un éventuel bug, ajouter la ligne suivante à l'intérieur du ficher Dockerfile qui se trouve à la racine du projet : 
`RUN chown -R www-data:www-data /var/www/html`


2-Lancer la commande Docker : 
`docker-compose up -d`

Docker va créer et démarrer 4 conteneurs par défault : db-headless, wp-headless, frontend & frontend-graphql.

3-Pour suivre le bon déroulement de la création des containers, lancer la commande :
`docker-compose logs -f`

En cas d'erreur liée au conteneur wp-headless, arrêter Docker avec docker-compose stop et lancer la commande: 
`RUN chown -R www-data:www-data /var/www/html`
Ensuite relancer Docker. 
Si l'erreur persiste, lancer la commande: 
`docker-compose up --build --force-recreate wp-headless`

4-Une fois le projet correctement installé, ajouter la libraire de GreenSock à l'intérieur du dossier frontend-graphql : 
`yarn add gsap`

Pour plus d'infos, consulter la documentation de Postlight : https://github.com/postlight/headless-wp-starter
